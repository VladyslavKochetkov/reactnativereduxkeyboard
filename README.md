# React Native Redux Keyboard

Package that is designed to work with redux. Initalize the listeners and use them all around this app.

This package exports the following

## Reducer

`import {Reducer} from "react-native-redux-keyboard";`

This is the reducer function to be added to redux.

```js
combineReducers({
    ...reducers,
    Keyboard: Reducer
});
```

## startListening ( *f* )

This starts listening to the keyboard events. This map be imported and called during the `index.js` file.

```js
import {startListening} from "react-native-redux-keyboard";
startListening();
```

## removeListeners ( *f* )

This removed all the event listeners and will no longer fire any dispatch events

```js
import {removeListeners} from "react-native-redux-keyboard";
removeListeners();
```

## setStore ( *f* )

This lets the package access redux store, it expects the paramater store, this should be called from the file that sets store.

```js
import { createStore } from "redux";
import {setStore} from "react-native-redux-keyboard";

const store = createStore(
    /* createStore login */
);

setStore(store);

export default store;
```

## Use Case

This is how it modified the state

```js
const Component = (props) => {
    useEffect(() => {
        console.log(props.Keyboard.eventType); // Prints event name
        console.log(props.Keyboard.keyboardEvent) // Prints event
    }, [props.Keyboard]);

    return {/* Render Logic */};
}

const mapStateToProps = state => ({
    Keyboard: ...state.Keyboard
});

export default connect(mapStateToProps, {})(Component);
```