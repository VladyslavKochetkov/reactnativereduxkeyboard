import {Keyboard, Dimensions} from "react-native";

let store = () => {};
const setStore = e => store = e;

const startListening = () => {
    Keyboard.addListener(
        "keyboardWillShow",
        native => store.dispatch({
            type: KEYBOARD_EVENT,
            payload: {
                type: "keyboardWillShow",
                native 
            }
        })
    );
    Keyboard.addListener(
        "keyboardDidShow",
        native => store.dispatch({
            type: KEYBOARD_EVENT,
            payload: {
                type: "keyboardDidShow",
                native 
            }
        })
    );
    Keyboard.addListener(
        "keyboardWillHide",
        native => store.dispatch({
            type: KEYBOARD_EVENT,
            payload: {
                type: "keyboardWillHide",
                native 
            }
        })
    );
    Keyboard.addListener(
        "keyboardDidHide",
        native => store.dispatch({
            type: KEYBOARD_EVENT,
            payload: {
                type: "keyboardDidHide",
                native 
            }
        })
    );
    Keyboard.addListener(
        "keyboardWillChangeFrame",
        native => store.dispatch({
            type: KEYBOARD_EVENT,
            payload: {
                type: "keyboardWillChangeFrame",
                native 
            }
        })
    );
    Keyboard.addListener(
        "keyboardDidChangeFrame",
        native => store.dispatch({
            type: KEYBOARD_EVENT,
            payload: {
                type: "keyboardDidChangeFrame",
                native 
            }
        })
    );
}

const KEYBOARD_EVENT = "RNReduxKeyboard";

const initialState = {
    eventType: undefined,
    keyboardEvent: {
        duration: 250,
        easing: "keyboard",
        endCoordinates: {
            height: 0,
            screenX: 0,
            screenY: Dimensions.get("window").height,
            width: Dimensions.get("window").width
        },
        isEventFromThisApp: false,
        startCoordinates: {
            height: 0,
            screenX: 0,
            screenY: Dimensions.get("window").height,
            width: Dimensions.get("window").width
        }
    }
}

const Reducer = (state = initialState, action) => {
    if(action.type === KEYBOARD_EVENT && action.payload.native){
        return {
            eventType: action.payload.type,
            keyboardEvent: action.payload.native
        }
    }
    return state;
}

const removeListeners = () => Keyboard.removeAllListeners();

module.exports = {
    Reducer,
    KEYBOARD_EVENT,
    startListening,
    removeListeners,
    setStore
}